//
//  RKViewController.swift
//  SquadGoals
//
//  Created by Tine Purg on 31/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct RKViewController: View {
    
    @ObservedObject var rkManager: RKManager
    @Binding var showMeetingAlert: Bool
    @Binding var selectedDate: Date
    
    var body: some View {
        Group {
            RKWeekdayHeader(rkManager: self.rkManager)
            Divider()
            List {
                ForEach(0..<numberOfMonths()) { index in
                    RKMonth(rkManager: self.rkManager, showMeetingAlert: self.$showMeetingAlert, selectedDate: self.$selectedDate, monthOffset: index)
                }
                Divider()
            }
        }
    }
    
    func numberOfMonths() -> Int {
        return rkManager.calendar.dateComponents([.month], from: rkManager.minimumDate, to: RKMaximumDateMonthLastDay()).month! + 1
    }
    
    func RKMaximumDateMonthLastDay() -> Date {
        var components = rkManager.calendar.dateComponents([.year, .month, .day], from: rkManager.maximumDate)
        components.month! += 1
        components.day = 0
        
        return rkManager.calendar.date(from: components)!
    }
}
