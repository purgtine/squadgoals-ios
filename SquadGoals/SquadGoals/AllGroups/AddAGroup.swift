//
//  AddAGroup.swift
//  SquadGoals
//
//  Created by Tine Purg on 10/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import Firebase
import CodableFirebase

struct AddAGroup: View {
    @State var  search: String
    @State var  groupType: String
    @State var  showModal = false
    @State var  showModal2 = false
    
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    var groups: [String : [GroupElement]] {
        if store.state.user != nil {
            return Dictionary(grouping: store.state.allGroups, by: { $0.groupType.rawValue.uppercased() })
        }
        return [:]
    }
    
    var uniqueGroups: [String] {
        groups.map({ $0.key }).sorted()
    }
    
    var body: some View {
        var ref: DatabaseReference!
        
        ref = Database.database().reference()
        
        let userId = store.state.user?.id
        
        ref.child("user").observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value else { return }
            do {
                let model = try FirebaseDecoder().decode([UserElement].self, from: value)
                
                var groups = [GroupElement]()
                
                for user in model {
                    if userId != user.id {
                        for group in user.groups {
                            if self.groupType == "" {
                                groups.append(group)
                            } else if group.groupType.rawValue == self.groupType {
                                groups.append(group)
                            }
                        }
                    }
                }
                
                self.store.dispatch(.fetchAllGroups(groups: groups))
            } catch let error {
                print(error)
            }
        }
        
        return VStack {
            Text("Results").padding(.top,10)
            List {
                ForEach(uniqueGroups, id: \.self) { group in
                    Section(header: Text(group)) {
                        ForEach(self.groups[group]!) { e in
                            NavigationLink(destination: GroupInfo(group: e, isUsers: false)) {
                                GroupRow(group: e)
                            }
                        }
                    }
                }
            }
            
            HStack {
                Spacer()
                Button(action: {
                    self.showModal.toggle()
                }) {
                    HStack {
                        Text("Search").frame(width: 100.0, height: 40.0)
                    }
                }.sheet(isPresented: $showModal) {
                    GroupModal(search: self.$search, groupType: self.$groupType, showModal: self.$showModal, addGroup: false)
                }
                .buttonStyle(GrayButtonStyle())
                Spacer()
                Button(action: {
                    self.showModal2.toggle()
                }) {
                    HStack {
                        Text("Add a group").frame(width: 100.0, height: 40.0)
                    }
                }.sheet(isPresented: $showModal2) {
                    GroupModal(search: self.$search, groupType: self.$groupType, showModal: self.$showModal2, addGroup: true).environmentObject(self.store)
                }.buttonStyle(GrayButtonStyle())
                Spacer()
            }.padding()
        }.navigationBarTitle(Text("All groups"))
    }
}

struct AddAGroup_Previews: PreviewProvider {
    static var previews: some View {
        AddAGroup(search: ".constant(",groupType: ".constant(", showModal: false)
    }
}
