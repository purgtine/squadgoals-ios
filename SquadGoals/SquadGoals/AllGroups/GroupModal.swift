//
//  GroupModal.swift
//  SquadGoals
//
//  Created by Tine Purg on 14/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//
import SwiftUI
import Firebase
import CodableFirebase

struct GroupModal: View {
    var strengths = ["Casual hangout", "Sport", "Event", "Online group"]
    var activity = ["hangout","sport","event","internet"]
    
    @State private var selectedStrength = 0
    @State private var nextPage = false
    @State private var lastPage = false
    
    @Binding var search: String
    @Binding var groupType: String
    @Binding var showModal: Bool
    @State var groupDescription: String = ""
    @State var groupName: String = ""
    
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    var addGroup: Bool
    
    var body: some View {
        let cgFloatPicker = 0.2*UIScreen.main.bounds.height
        let cgFloatButton = 0.25*UIScreen.main.bounds.height
        
        return VStack {
            if nextPage == false {
                Form {
                    Picker(selection: $selectedStrength, label: Text("")) {
                        ForEach(0 ..< strengths.count) {
                            Text(self.strengths[$0])
                        }
                    }.padding(.top,cgFloatPicker).pickerStyle(WheelPickerStyle())
                   
                    HStack {
                        Spacer()
                        Button(action: {
                            if self.activity[self.selectedStrength] != "hangout" {
                                self.nextPage.toggle()
                            } else if self.addGroup == false {
                                self.showModal.toggle()
                                self.groupType = self.activity[self.selectedStrength]
                            } else {
                                self.nextPage.toggle()
                                self.groupType = self.activity[self.selectedStrength]
                            }
                        }) {
                            HStack {
                                if self.activity[self.selectedStrength] != "hangout" || self.addGroup == true  {
                                    Text("Next").frame(width: 100.0, height: 40.0)
                                }
                                else {
                                    Text("Search").frame(width: 100.0, height: 40.0)
                                }
                            }
                        }
                        Spacer()
                    }.buttonStyle(GrayButtonStyle()).padding(.top, cgFloatButton)
                }
            } else if lastPage == false {
                Form {
                    if self.addGroup == true {
                    TextField("Enter a name for the group", text: self.$groupName)
                    }
                    if activity[selectedStrength] == "event" {
                        TextField("Enter an event", text: self.$search)
                    } else if activity[selectedStrength] == "sport" {
                        TextField("Enter a sport", text: self.$search)
                    } else if activity[selectedStrength] == "internet" {
                        TextField("Enter a game", text: self.$search)
                    }
                    }.padding(.top,0.35*UIScreen.main.bounds.height)
                
                Button(action: {
                    if self.addGroup == true {
                        self.lastPage = true
                    } else {
                        self.showModal.toggle()
                    }
                    self.groupType = self.activity[self.selectedStrength]
                }) {
                    if self.addGroup == false {
                        Text("Search").frame(width: 100.0, height: 40.0)
                    } else {
                        Text("Next").frame(width: 100.0, height: 40.0)
                    }
                }.buttonStyle(GrayButtonStyle())
                    
                Spacer()
            } else {
                Text("Enter a description").padding(20)
                HStack {
                MultilineTextView(text: $groupDescription).font(.largeTitle).border(Color.black)
                }.padding(40)
                Button(action: {
                    var ref: DatabaseReference!
                    
                    ref = Database.database().reference()
                    
                    var groups = self.store.state.user?.groups
                    
                    var groupType: GroupType = .event
                    
                    switch (self.activity[self.selectedStrength]) {
                    case "hangout":
                        groupType = .hangout
                        
                    case "sport":
                        groupType = .sport
                        
                    case "internet":
                        groupType = .internet
                
                    default:
                        groupType = .event
                    }
                    
                    let group = GroupElement(name: self.groupName, groupType: groupType, groupDescription: self.groupDescription)
                    
                    groups?.append(group)
                    
                    let count = groups!.count
                    
                    do {
                        
                    let model = try FirebaseEncoder().encode(group)
                    
                        ref.child("user").child("0").child("groups").child("\(count)").setValue(["\(count)":model])
                        
                    } catch {
                        
                    }
                }) {
                    Text("Add group").frame(width: 100.0, height: 40.0)
                }.buttonStyle(GrayButtonStyle()).padding(.bottom, 20)
            }
        }
    }
}

struct GroupModal_Previews: PreviewProvider {
    static var previews: some View {
        GroupModal(search: .constant(""), groupType: .constant(""), showModal: .constant(true), addGroup: false)
    }
}
