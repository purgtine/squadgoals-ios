//
//  Locations.swift
//  SquadGoals
//
//  Created by Tine Purg on 14/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import MapKit

struct Locations: View {
    var group: GroupElement
    
    @EnvironmentObject private var store: Store<AppState, AppAction>
        
    var body: some View {
        return VStack {
            Text("Suggested locations")
            List(group.location) { location in
                NavigationLink(destination: MapContainer(location: location).environmentObject(self.store)) {
                     LocationRow(location: location)
                }
            }
            HStack {
                Spacer()
                Button(action: {
                    print("Button action")
                }) {
                    HStack {
                        Text("Vote").frame(width: 100.0, height: 40.0)
                    }
                }.buttonStyle(GrayButtonStyle())
                Spacer()
            }.padding(.bottom, 20)
            Spacer()
           }.padding(.top,20)
            .navigationBarTitle("\(group.name)", displayMode: .inline)
            .navigationBarItems(trailing:
                NavigationLink(destination: LocationPicker(group: self.group).environmentObject(store)) {
            HStack {
                Image(systemName: "plus.circle.fill").resizable()
                
                }
            }
        )
    }
}
