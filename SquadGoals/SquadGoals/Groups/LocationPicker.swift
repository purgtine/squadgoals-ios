//
//  LocationPicker.swift
//  SquadGoals
//
//  Created by Tine Purg on 23/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import MapKit
import Firebase
import CodableFirebase

struct LocationPicker: View {
    var group: GroupElement
    
    @ObservedObject private var locationManager = LocationManager()
    @State private var showConfirmationAlert = false
    @State private var search: String = ""
    @State private var landmarks: [Landmark] = [Landmark]()
    @State private var tapped: Bool = false
    @State private var landmark: Landmark?
    
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    private func getNearByLandmarks() {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = search
        
        let search = MKLocalSearch(request: request)
        search.start { (response, error) in
            if let response = response {
                let mapItems = response.mapItems
                self.landmarks = mapItems.map {
                    Landmark(placemark: $0.placemark)
                }
            }
        }
    }
    
    func calculateOffset() -> CGFloat {
        if self.landmarks.count > 0 && !self.tapped {
            return UIScreen.main.bounds.size.height - UIScreen.main.bounds.size.height / 4
        } else if self.tapped {
            return 100
        } else {
            return UIScreen.main.bounds.size.height
        }
    }
    
    var body: some View {
        return ZStack(alignment: .top){
            MapView(showConfirmationAlert: $showConfirmationAlert, landmarks: landmarks)
            TextField("Search", text: $search, onEditingChanged: {
                _ in })
            {
                self.getNearByLandmarks()
            }.textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
                .offset(y: 20)
            
            PlaceListView(landmarks: landmarks, showConfirmation: $showConfirmationAlert, landmark: $landmark) {
                self.tapped.toggle()
            }.animation(.spring()).offset(y:calculateOffset())
            
        }.alert(isPresented: $showConfirmationAlert) { () -> Alert in
            Alert(title: Text("Confirmation alert"), message: Text("Are you sure you want to add this location? \(self.landmark?.title ?? "") "), primaryButton:.cancel(), secondaryButton: .default(Text("Add location"), action: {
                var ref: DatabaseReference!
                
                ref = Database.database().reference()
                
                let groups = self.store.state.user?.groups
                
                var count = 0
                
                for (i,group) in groups!.enumerated() {
                    if group.id == self.group.id {
                        count = i
                        break
                    }
                }
                
                let countLocation = self.group.location.count
                
                guard let key = ref.child("user").child("0").child("groups").child("\(count)").child("location").child("\(countLocation)").key else { return }
                
                do {
                    let meeting = Location(landmark: self.landmark!)
                    let model = try FirebaseEncoder().encode(meeting)
                    
                    let childUpdates = ["user/0/groups/\(count)/location/\(key)": model]
                    ref.updateChildValues(childUpdates as [AnyHashable : Any])
                } catch {
                    
                }
            }))
        }
    }
}
