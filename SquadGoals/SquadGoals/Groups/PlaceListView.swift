//
//  PlaceListView.swift
//  SquadGoals
//
//  Created by Tine Purg on 29/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import MapKit

struct PlaceListView: View {
    var landmarks: [Landmark]
    @Binding var showConfirmation: Bool
    @Binding var landmark: Landmark?
    var onTap: () -> ()
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                EmptyView()
            }.frame(width: UIScreen.main.bounds.size.width, height:60)
                .background(Color.gray)
                .gesture(TapGesture().onEnded(self.onTap))
            
            List {
                ForEach(self.landmarks, id: \.id) { landmark in
                    LandmarkRow(landmark: landmark, landmarkState: self.$landmark, showConfirmation: self.$showConfirmation)
                }
            }.animation(nil)
        }.cornerRadius(10)
    }
}
