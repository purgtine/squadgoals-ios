//
//  MeetingRow.swift
//  SquadGoals
//
//  Created by Tine Purg on 13/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct MeetingRow: View {
     var meeting: Meeting

       var body: some View {
        
        let meetingString = meeting.date.split(separator: "-")
        
        let date = "\(meetingString[0]). \(meetingString[1]).  \(meetingString[2])  \(meetingString[3]):\(meetingString[4])"
        
        return HStack {
            CheckboxField(
                id: "\(meeting.id)",
                label: date,
                callback: checkboxSelected)
            Text("Votes: \(meeting.votes)")
               Spacer()
           }
       }
    
    func checkboxSelected(id: String, isMarked: Bool) {
        print("\(id) is marked: \(isMarked)")
    }
}
