//
//  LandmarkRow.swift
//  SquadGoals
//
//  Created by Tine Purg on 29/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct LandmarkRow: View {
    var landmark: Landmark
    @Binding var landmarkState: Landmark?
    @Binding var showConfirmation: Bool
    
    var body: some View {
        Button(action: {
            self.landmarkState = self.landmark
            self.showConfirmation.toggle()
        }) {
            VStack(alignment: .leading) {
                Text(landmark.name)
                    .fontWeight(.bold)
                Text(landmark.title)
            }
        }
    }
}
