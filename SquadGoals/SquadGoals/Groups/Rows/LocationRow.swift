//
//  LocationRow.swift
//  SquadGoals
//
//  Created by Tine Purg on 14/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct LocationRow: View {
       var location: Location

        var body: some View {
            HStack {
             CheckboxField(id: "\(location.id)",label: "\(location.city)",callback: checkboxSelected)
                Text("Votes: \(location.votes)")
                Spacer()
            }
        }
     
     func checkboxSelected(id: String, isMarked: Bool) {
         print("\(id) is marked: \(isMarked)")
     }
}
