//
//  ChatRow.swift
//  SquadGoals
//
//  Created by Tine Purg on 02/08/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct ChatRow: View {
      var group: GroupElement

      var body: some View {
          HStack {
              Text(group.name)
              Spacer()
          }
      }
}
