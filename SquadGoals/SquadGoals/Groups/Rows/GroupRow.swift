//
//  GroupRow.swift
//  SquadGoals
//
//  Created by Tine Purg on 09/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct GroupRow: View {
    var group: GroupElement

    var body: some View {
        HStack {
            Text(group.name)
            Spacer()
        }
    }
}
