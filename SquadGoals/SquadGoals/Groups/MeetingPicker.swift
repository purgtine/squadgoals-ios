//
//  GroupCalendarPicker.swift
//  SquadGoals
//
//  Created by Tine Purg on 23/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import Firebase
import CodableFirebase

struct CalendarPicker: View {
    var group: GroupElement
    @State var selectedDate = Date()
  
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    var body: some View {
        VStack {
            Text("Choose a date")
            DatePicker("", selection: $selectedDate).padding(40)
            Button(action: {
                var ref: DatabaseReference!
                
                ref = Database.database().reference()
                
                let groups = self.store.state.user?.groups
                
                var count = 0
                
                for (i,group) in groups!.enumerated() {
                    if group.id == self.group.id {
                        count = i
                        break
                    }
                }
                
                let countMeeting = self.group.meeting.count
                print("Count \(countMeeting)")
                
                guard let key = ref.child("user").child("0").child("groups").child("\(count)").child("meeting").child("\(countMeeting)").key else { return }
                
                do {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let myString = formatter.string(from: self.selectedDate)
                    let yourDate = formatter.date(from: myString)
                    formatter.dateFormat = "dd-MMM-yyyy-HH-mm"
                    let myStringafd = formatter.string(from: yourDate!)
                    
                    let meeting = Meeting(id: UUID().description,date: myStringafd, votes: 0)
                    let model = try FirebaseEncoder().encode(meeting)
                    
                    let childUpdates = ["user/0/groups/\(count)/meeting/\(key)": model]
                    ref.updateChildValues(childUpdates as [AnyHashable : Any])
                } catch {
                    
                }
            }) {
                Text("Add date").frame(width: 100.0, height: 40.0)
            }.buttonStyle(GrayButtonStyle())
        }
    }
}
