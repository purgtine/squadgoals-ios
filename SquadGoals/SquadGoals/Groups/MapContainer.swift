//
//  MapContainer.swift
//  SquadGoals
//
//  Created by Tine Purg on 28/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import MapKit

struct MapContainer: View {
    var location: Location
    
    var body: some View {
        let annotation: MKPointAnnotation = MKPointAnnotation()
        annotation.title = location.city
        let latitude: Double = location.latitude
        let longitude: Double = location.longitude
           annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        
        return Map(annotation: annotation)
    }
}

