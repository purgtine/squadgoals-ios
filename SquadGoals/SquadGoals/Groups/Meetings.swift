//
//  Meetings.swift
//  SquadGoals
//
//  Created by Tine Purg on 13/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct Meetings: View {
    var group: GroupElement
    
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    var body: some View {
       VStack {
            Text("Suggested times")
            List(group.meeting) { meeting in
                MeetingRow(meeting: meeting)
            }
        HStack {
            Spacer()
            Button(action: {
                print("Button action")
            }) {
                HStack {
                    Text("Vote").frame(width: 100.0, height: 40.0)
                    }
            }.buttonStyle(GrayButtonStyle())
            Spacer()
        }.padding(.bottom, 20)
        Spacer()
       }.padding(.top,20)
        .navigationBarTitle("\(group.name)", displayMode: .inline)
        .navigationBarItems(trailing:
            NavigationLink(destination: CalendarPicker(group: self.group).environmentObject(store)) {
            HStack {
                Image(systemName: "plus.circle.fill").resizable()
                
                }
            }
        )
        }
    }

struct GrayButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .foregroundColor(Color.white)
            .background(Color.gray)
            .cornerRadius(10.0)
    }
}
/**
struct Meetings_Previews: PreviewProvider {
    static var previews: some View {
        Meetings()
    }
}
*/
