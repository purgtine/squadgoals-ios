//
//  GroupInfo.swift
//  SquadGoals
//
//  Created by Tine Purg on 17/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import Firebase
import CodableFirebase

struct GroupInfo: View {
    var group: GroupElement
    var isUsers: Bool
    
     @EnvironmentObject private var store: Store<AppState, AppAction>
    
    var body: some View {
        VStack(alignment: .leading) {
            if isUsers == false {
                Button(action: {
                    
                    var ref: DatabaseReference!
                    
                    ref = Database.database().reference()
                        
                    var groups = self.store.state.user?.groups
                    groups?.append(self.group)
                    
                    let count = groups!.count - 1
                    
                    guard let key = ref.child("user").child("0").child("groups").child("\(count)").key else { return }
                    do {
                        let model = try FirebaseEncoder().encode(self.group)
                          
                        let childUpdates = ["user/0/groups/\(key)": model]
                        ref.updateChildValues(childUpdates as [AnyHashable : Any])
                    } catch {
                        
                    }
                    
                }) {
                    HStack(alignment: .center) {
                        Text("Join").frame(width: 100.0, height: 40.0)
                    }
                }.buttonStyle(GrayButtonStyle())
            } else {
                Button(action: {
                    print("Button action")
                }) {
                    HStack {
                        Text("Add a group").frame(width: 100.0, height: 40.0)
                    }
                    }.buttonStyle(GrayButtonStyle()).hidden()
            }
            HStack {
                Text("Group type: ").bold()
                Text("\(group.groupType.rawValue)")
            }.padding()
            Text("Group description: ").bold().padding()
            Text("\(group.groupDescription)").padding()
            Spacer()
        }.navigationBarTitle("\(group.name)").padding(.top, 20)
    }
}
