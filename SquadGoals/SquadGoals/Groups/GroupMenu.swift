//
//  GroupMenu.swift
//  SquadGoals
//
//  Created by Tine Purg on 10/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import Firebase
import CodableFirebase

struct GroupMenu: View {
    var group: GroupElement
    
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    var body: some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                NavigationLink(destination: Locations(group: group).environmentObject(store)) {
                        HStack {
                            Image(systemName: "location.fill").resizable()
                            .frame(width: 32.0, height: 32.0)
                        }
                }.buttonStyle(GradientButtonStyle())
                Spacer()
                Button(action: {
                          print("Button action")
                      }) {
                          HStack {
                              Image(systemName: "message.fill").resizable()
                              .frame(width: 32.0, height: 32.0)
                          }
                      }.buttonStyle(GradientButtonStyle())
                Spacer()
            }.padding(.bottom,20)
            HStack {
                Spacer()
                NavigationLink(destination: Meetings(group: group).environmentObject(store)) {
                        HStack {
                            Image(systemName: "calendar").resizable()
                                .frame(width: 32.0, height: 32.0)
                                        }
                }.buttonStyle(GradientButtonStyle())
                Spacer()
                NavigationLink(destination: GroupInfo(group: group, isUsers: true).environmentObject(self.store)) {
                        HStack {
                            Image(systemName: "info.circle.fill").resizable()
                            .frame(width: 32.0, height: 32.0)
                        }
                }.buttonStyle(GradientButtonStyle())
                Spacer()
                
            }.padding(.top,20)
            Spacer()
        }.navigationBarTitle("\(group.name)", displayMode: .inline)
    }
}

struct GradientButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .foregroundColor(Color.white)
            .padding(50)
            .background(Color.red)
            .cornerRadius(15.0)
    }
}

/**
struct GroupMenu_Previews: PreviewProvider {
    static var previews: some View {
        GroupMenu(group: group)
    }
}
*/

struct GroupMenu_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
