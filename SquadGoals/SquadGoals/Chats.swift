//
//  Chat.swift
//  SquadGoals
//
//  Created by Tine Purg on 16/06/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI
import FirebaseDatabase
import CodableFirebase

struct Chats: View {
   @EnvironmentObject private var store: Store<AppState, AppAction>
        
        var groups: [String : [GroupElement]] {
            if store.state.user != nil {
                return Dictionary(grouping: store.state.user!.groups, by: { $0.groupType.rawValue.uppercased() })
            }
            return [:]
        }
        
        var uniqueGroups: [String] {
            groups.map({ $0.key }).sorted()
        }
        
        var body: some View {
           var ref: DatabaseReference!
            
            ref = Database.database().reference()
            
            ref.child("user").child("0").observeSingleEvent(of: .value, with: { (snapshot) in
                guard let value = snapshot.value else { return }
                do {
                    let model = try FirebaseDecoder().decode(UserElement.self, from: value)
                    self.store.dispatch(.fetchUser(user: model))
                    
                } catch let error {
                    print(error)
                }
            }) { (error) in
                print(error.localizedDescription)
            }
            
            return  VStack() {
                Text("My chats").bold().padding()
                Spacer()
                if store.state.user != nil {
                    List {
                        ForEach(uniqueGroups, id: \.self) { group in
                            Section(header: Text(group)) {
                                ForEach(self.groups[group]!) { e in
                                    NavigationLink(destination: ChatScreen(group: e).environmentObject(self.store)) {
                                        ChatRow(group: e)
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    Text("Nothing here")
                }
            } .navigationBarTitle("My groups", displayMode: .inline)
        }
    }

