// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let user = try? newJSONDecoder().decode(User.self, from: jsonData)

import Foundation

// MARK: - User
struct User: Codable {
    let user: [UserElement]
}

// MARK: - UserElement
struct UserElement: Codable {
    let id, gender: String
    let name: NameClass
    let password, email: String
    let groups: [GroupElement]

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case gender, name, password, email, groups
    }
}

// MARK: - Group
struct GroupElement: Codable, Identifiable {
    let id, name: String
    let location: [Location]
    let meeting: [Meeting]
    let groupType: GroupType
    let groupDescription: String
    let chat: [Chat]

    init(name: String, groupType: GroupType, groupDescription: String) {
        self.location = []
        self.meeting = []
        self.chat = []
        self.groupType = groupType
        self.name = name
        self.groupDescription = groupDescription
        self.id = UUID().description
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name, location, meeting, groupType
        case groupDescription = "description"
        case chat
    }
}

// MARK: - Chat
struct Chat: Codable {
    let message: [Message]
}

// MARK: - Message
struct Message: Codable {
    let date, message: String
}

enum GroupType: String, Codable {
    case event = "event"
    case hangout = "hangout"
    case internet = "internet"
    case sport = "sport"
}

// MARK: - Location
struct Location: Codable, Identifiable {
    let id: String
    let name: NameEnum
    let street, city, country: String
    let latitude, longitude: Double
    let votes: Int
    
    init(landmark: Landmark) {
        self.id = UUID().description
        self.street = landmark.name
        self.city = landmark.name
        self.country = landmark.name
        self.latitude = landmark.coordinate.latitude
        self.longitude = landmark.coordinate.longitude
        self.name = .nekaj
        self.votes = 0
    }
}

enum NameEnum: String, Codable {
    case nekaj = "nekaj"
}

// MARK: - Meeting
struct Meeting: Codable, Identifiable {
    let id, date: String
    let votes: Int
    
    init(id: String,date: String, votes: Int) {
        self.id = id
        self.votes = votes
        self.date = date
    }
}

// MARK: - NameClass
struct NameClass: Codable {
    let first, last: String
}
