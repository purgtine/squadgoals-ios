//
//  LandmarkAnnotation.swift
//  SquadGoals
//
//  Created by Tine Purg on 29/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import Foundation
import MapKit

final class LandmarkAnnotation: NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let title: String?
    
    init(landmark: Landmark) {
        self.title = landmark.name
        self.coordinate = landmark.coordinate
    }
}
