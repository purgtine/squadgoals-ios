//
//  Landmark.swift
//  SquadGoals
//
//  Created by Tine Purg on 29/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import Foundation
import MapKit

struct Landmark {
    let placemark: MKPlacemark
    
    var id: UUID {
        return UUID()
    }
    
    var name: String {
        self.placemark.title ?? ""
    }
    
    var title: String {
        self.placemark.title ?? ""
    }
    
    var coordinate: CLLocationCoordinate2D {
        self.placemark.coordinate
    }
}
