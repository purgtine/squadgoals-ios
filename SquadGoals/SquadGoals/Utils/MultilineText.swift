//
//  MultilineText.swift
//  SquadGoals
//
//  Created by Tine Purg on 21/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import Foundation
import SwiftUI

struct MultilineTextView: UIViewRepresentable {
    @Binding var text: String

    func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.isScrollEnabled = true
        view.isEditable = true
        view.isUserInteractionEnabled = true
        return view
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.text = text
    }
}
