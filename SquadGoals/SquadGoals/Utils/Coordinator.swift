//
//  Coordinator.swift
//  SquadGoals
//
//  Created by Tine Purg on 23/07/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import Foundation
import MapKit

final class Coordinator: NSObject, MKMapViewDelegate {
    var control: MapView
    private let locationManager = CLLocationManager()
    @Published var location: CLLocation? = nil
    
    init(_ control: MapView) {
        self.control = control
    }
  
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let annotationView = views.first {
            if let annotation = annotationView.annotation {
                if annotation is MKUserLocation {
                    let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
                    mapView.setRegion(region, animated: true)
                    
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
              
              guard let location = locations.last else {
                  return
              }
              
              self.location = location
          
          }
}
