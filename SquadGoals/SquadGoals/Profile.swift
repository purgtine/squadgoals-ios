//
//  Profile.swift
//  SquadGoals
//
//  Created by Tine Purg on 22/06/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct Profile: View {
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    var body: some View {
        VStack() {
            Text("User profile").bold().padding()
            
            Image(systemName: "person.circle").resizable()
                .frame(width: 60.0, height: 60.0)
                .foregroundColor(.red)
            Spacer()
            VStack(alignment: .leading){
                HStack {
                    Text("Name:").bold()
                    Text("\(store.state.user?.name.first ?? "")")
                }
                HStack {
                    Text("Surname:").bold()
                    Text("\(store.state.user?.name.last  ?? "")")
                }
                HStack {
                    Text("Email:").bold()
                    Text("\(store.state.user?.email ?? "")")
                }
            }.padding(.bottom, 50)
        }
    }
}
