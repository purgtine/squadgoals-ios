//
//  ContentView.swift
//  SquadGoals
//
//  Created by Tine Purg on 14/05/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    @State private var showModal = false
    
    var body: some View {
        NavigationView {
            TabView {
                Groups().tabItem {
                    Image(systemName: "rectangle.stack")
                    Text("Groups")
                }.environmentObject(store)
                
                CalendarView().tabItem {
                    Image(systemName: "calendar")
                    Text("Calendar")
                }.environmentObject(store)
                
                Profile().tabItem {
                    Image(systemName: "person")
                    Text("Profile")
                }.environmentObject(store)
                
                Chats().tabItem {
                    Image(systemName: "message")
                    Text("Chat")
                }.environmentObject(store)
            }
            .accentColor(.red)
            .navigationBarTitle("SquadGoals", displayMode: .inline)
            .navigationBarItems(trailing:
                NavigationLink(destination: AddAGroup(search: "", groupType: "").environmentObject(store)) {
                HStack {
                    Image(systemName: "plus.circle.fill").resizable()
                    
                    }
                }
            )
            .background(NavigationConfigurator { nc in
                nc.navigationBar.barTintColor = .red
                nc.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
                nc.navigationBar.tintColor = UIColor.white
            })
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
