//
//  Calendar.swift
//  SquadGoals
//
//  Created by Tine Purg on 16/06/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct CalendarView: View {
    @EnvironmentObject private var store: Store<AppState, AppAction>
    
    @State private var showMeetingAlert: Bool = false
    @State private var selectedDate = Date()
    
    var rkManager = RKManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 3)
    
    var body: some View {
        return VStack (spacing: 25) {
            RKViewController(rkManager: self.rkManager, showMeetingAlert: $showMeetingAlert, selectedDate: $selectedDate)
            datesView(dates: self.rkManager.selectedDates)
        }.onAppear(perform: startUp)
            .navigationViewStyle(StackNavigationViewStyle())
        .alert(isPresented: $showMeetingAlert) { () -> Alert in
            Alert(title: Text("Your selected date"), message: Text(""), primaryButton:.cancel(), secondaryButton: .default(Text("Add location")))}
    }
    
    func datesView(dates: [Date]) -> some View {
        ScrollView (.horizontal) {
            HStack {
                ForEach(dates, id: \.self) { date in
                    Text(self.getTextFromDate(date: date))
                }
            }
        }.padding(.horizontal, 15)
    }
    
    func startUp() {
        // example of pre-setting selected dates
        
        var setup = [Date]()
        
        let groups = store.state.user!.groups
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy-HH-mm-ss"
        
        for group in groups {
            for meeting in group.meeting {
                let date = formatter.date(from: meeting.date)
                setup.append(date!)
            }
        }
        
        rkManager.selectedDates.append(contentsOf: setup)
        
        // example of some foreground colors
        rkManager.colors.weekdayHeaderColor = Color.blue
        rkManager.colors.monthHeaderColor = Color.green
        rkManager.colors.textColor = Color.blue
        rkManager.colors.disabledColor = Color.red
        
    }
    
    func getTextFromDate(date: Date!) -> String {
        let formatter = DateFormatter()
        formatter.locale = .current
        formatter.dateFormat = "EEEE, MMMM d, yyyy"
        return date == nil ? "" : formatter.string(from: date)
    }
}
