//
//  Copyright © 2016, Connected Travel, LLC – All Rights Reserved. 
//
//  All information contained herein is property of Connected Travel, LLC including, but 
//  not limited to, technical and intellectual concepts which may be embodied within. 
//
//  Dissemination or reproduction of this material is strictly forbidden unless prior written
//  permission, via license, is obtained from Connected Travel, LLC.   If permission is obtained,
//  this notice, and any other such legal notices, must remain unaltered.  
//

import Foundation
import Combine

class AppState: ObservableObject {
    @Published var user: UserElement? = nil
    @Published var allGroups: [GroupElement] = []
}

struct Reducer<State, Action> {
    typealias Change = (inout State) -> Void
    let reduce: (State, Action) -> AnyPublisher<Change, Never>
}

extension Reducer {
    static func sync(_ fun: @escaping (inout State) -> Void) -> AnyPublisher<Change, Never> {
        Just(fun).eraseToAnyPublisher()
    }
}

class ReducerHelper {
    func appReducer() -> Reducer<AppState,AppAction> {
        return Reducer { state, action in
            var userTemp = state.user
            var allGroups = state.allGroups
            
            switch action {
            case .fetchUser(let user):
                userTemp = user

            case .fetchAllGroups(let groups):
                allGroups = groups
                
            }
            
            return Reducer<AppState, AppAction>.sync { state in
                state.user = userTemp
                state.allGroups = allGroups
            }
        }
    }
}
