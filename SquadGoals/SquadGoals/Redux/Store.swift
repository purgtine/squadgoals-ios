//
//  Copyright © 2016, Connected Travel, LLC – All Rights Reserved. 
//
//  All information contained herein is property of Connected Travel, LLC including, but 
//  not limited to, technical and intellectual concepts which may be embodied within. 
//
//  Dissemination or reproduction of this material is strictly forbidden unless prior written
//  permission, via license, is obtained from Connected Travel, LLC.   If permission is obtained,
//  this notice, and any other such legal notices, must remain unaltered.  
//

import Foundation
import Combine

typealias Dispatcher = (AppAction) -> Void

final class Store<State, Action>: ObservableObject {
    @Published private(set) var state: State
    
    private let reducer: Reducer<State, Action>
    private var reducerCancelable: AnyCancellable?
    
    init(initialState: State, reducer: Reducer<State, Action>) {
        self.state = initialState
        self.reducer = reducer
    }
    
    func dispatch(_ action: Action) {
        self.reducerCancelable = reducer
            .reduce(state, action)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: perform)
    }
    
    private func perform(change: Reducer<State, Action>.Change) {
        change(&state)
        reducerCancelable?.cancel()
    }
}
