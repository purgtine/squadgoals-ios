//
//  NavigationBar.swift
//  SquadGoals
//
//  Created by Tine Purg on 16/06/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import UIKit
import SwiftUI

struct NavigationConfigurator: UIViewControllerRepresentable {
     var configure: (UINavigationController) -> Void = { _ in }

     func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
         UIViewController()
     }
    
     func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
         if let nc = uiViewController.navigationController {
             self.configure(nc)
         }
     }
 }
