//
//  ChatScreen.swift
//  SquadGoals
//
//  Created by Tine Purg on 02/08/2020.
//  Copyright © 2020 Tine Purg. All rights reserved.
//

import SwiftUI

struct ChatScreen: View {
    var group: GroupElement
    
    @State private var chatField: String = ""
    
    var body: some View {
        VStack{
            Text("Hello, World!")
            Spacer()
            TextField("", text: $chatField)
        }
    }
}

